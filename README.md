# Python template

Sample Python Template with Poetry.

Includes these technologies too:

- `mypy` - A python static type checker
- `black` - A python code formatter
- `isort` - A python imports formatter
- ~~`flake8`~~ - _Soon_

Built for Python 3.10.

(Might make this a cookiecutter template later)

## How to use

### With Degit

Use the Degit tool (on [NPM](https://npmjs.com/package/degit)) to clone this project (and remove the git repo automatically).

```bash
npx degit https://gitlab.com/arnu515-tutorials/starters/python my-app  # requires NodeJS installed

cd my-app  # cd into your new project

git init  # re-initialise a git repo
```

### With Git

```bash
git clone https://gitlab.com/arnu515-tutorials/starters/python my-app  # requires git installed

cd my-app  # cd into your project

rm -rf .git && git init  # delete current git repo and create a new one
```
